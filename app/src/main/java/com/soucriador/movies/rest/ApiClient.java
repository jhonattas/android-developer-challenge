package com.soucriador.movies.rest;

import android.util.Log;

import com.soucriador.movies.BuildConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    // api própria que busca as listas de filmes e series favoritos
    private static final String BASE_CRIADOR_URL = BuildConfig.api_criador;

    private static Retrofit retrofitCriador = null;

    public static Retrofit getCriadorClient() {
        if(retrofitCriador == null) {
            retrofitCriador = new Retrofit.Builder()
                    .baseUrl(BASE_CRIADOR_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitCriador;
    }
}
