package com.soucriador.movies.rest;

import com.soucriador.movies.model.About;
import com.soucriador.movies.model.Show;
import com.soucriador.movies.model.ShowIMDB;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET("movies")
    Call<ArrayList<Show>> getAllMovies();

    @GET
    Call<ShowIMDB> getMovie(@Url String fullUrl);

    @GET("series")
    Call<ArrayList<Show>> getAllSeries();

    @GET("about")
    Call<About> getAbout();
}
