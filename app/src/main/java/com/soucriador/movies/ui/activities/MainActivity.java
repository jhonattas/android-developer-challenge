package com.soucriador.movies.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.soucriador.movies.R;
import com.soucriador.movies.model.Show;
import com.soucriador.movies.model.ShowIMDB;
import com.soucriador.movies.ui.fragments.MoviesFragment;
import com.soucriador.movies.ui.fragments.SeriesFragment;
import com.soucriador.movies.ui.interfaces.OnFragmentInteractionListener;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    private ActionBar toolbar;
    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_movies:
                    // mTextMessage.setText(R.string.title_home);

                    toolbar.setTitle("Filmes");
                    loadFragment(MoviesFragment.newInstance("", ""));
                    return true;
                case R.id.navigation_series:
                    toolbar.setTitle("Séries");
                    loadFragment(SeriesFragment.newInstance("", ""));
                    return true;
                case R.id.navigation_about:
                    // mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextMessage = findViewById(R.id.message);
        toolbar = getSupportActionBar();

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // attaching bottom sheet behaviour - hide / show on scroll
        //CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        // layoutParams.setBehavior(new BottomNavigationBehavior());

        // load the store fragment by default
        toolbar.setTitle("Filmes");
        loadFragment(MoviesFragment.newInstance("", ""));
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(Object obj) {
        Show show = (Show) obj;
        Intent i = new Intent(this, ShowDetailsActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("show", show);
        i.putExtras(b);
        startActivity(i);
    }

}
