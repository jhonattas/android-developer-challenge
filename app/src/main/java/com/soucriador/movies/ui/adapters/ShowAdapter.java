package com.soucriador.movies.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.soucriador.movies.R;
import com.soucriador.movies.model.Show;
import com.soucriador.movies.ui.interfaces.OnFragmentInteractionListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ShowAdapter extends RecyclerView.Adapter<ShowAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<Show> showList;
    private OnFragmentInteractionListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, price;
        ImageView thumbnail;
        CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.card_view);
            name = view.findViewById(R.id.title);
            price = view.findViewById(R.id.price);
            thumbnail = view.findViewById(R.id.thumbnail);
        }
    }


    public ShowAdapter(Context context, ArrayList<Show> showList, OnFragmentInteractionListener listener) {
        this.context = context;
        this.showList = showList;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_item_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Show show = showList.get(position);
        holder.name.setText(show.getTitle());

        Picasso.get()
                .load(show.getPoster())
                .fit()
                .into(holder.thumbnail);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != listener) {
                    listener.onFragmentInteraction(show);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return showList.size();
    }
}
