package com.soucriador.movies.ui.interfaces;

public interface OnFragmentInteractionListener {
    public void onFragmentInteraction(Object object);
}
