package com.soucriador.movies.ui.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.soucriador.movies.BuildConfig;
import com.soucriador.movies.R;
import com.soucriador.movies.model.Show;
import com.soucriador.movies.model.ShowIMDB;
import com.soucriador.movies.rest.ApiClient;
import com.soucriador.movies.rest.ApiInterface;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowDetailsActivity extends AppCompatActivity {

    private Show show;
    private ShowIMDB showIMDB;
    private TextView tvPlot;
    private TextView tvGenre;
    private TextView tvRuntime;
    private Toolbar toolbar;
    private ImageView movieSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle b = getIntent().getExtras();
        this.show = (Show) b.getSerializable("show");
        fetchIMDBShow(this.show.getImdbID());
        setTitle(this.show.getTitle());

        onCreateComponents();
    }

    void onCreateComponents(){
        movieSlider = findViewById(R.id.movieSlider);
        tvPlot      = findViewById(R.id.tv_show_plot);
        tvGenre     = findViewById(R.id.tv_show_genre);
        tvRuntime   = findViewById(R.id.tv_show_runtime);

    }

    private void fetchIMDBShow(String showID){
        ApiInterface apiService = ApiClient.getCriadorClient().create(ApiInterface.class);
        String fullUrl = BuildConfig.api_omdb + "?i=" + showID + "&apikey=" + BuildConfig.omdb_apikey;
        Log.e("ShowDetails", fullUrl);

        Call<ShowIMDB> call = apiService.getMovie(fullUrl);
        call.enqueue(new Callback<ShowIMDB>() {
            @Override
            public void onResponse(Call<ShowIMDB> call, Response<ShowIMDB> response) {
                if(response.body() != null) {
                    showIMDB = response.body();
                    populateShow();
                }
            }

            @Override
            public void onFailure(Call<ShowIMDB> call, Throwable t) {
                Toast.makeText(ShowDetailsActivity.this, " " + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void populateShow(){
        Picasso.get().load(showIMDB.getPoster()).into(movieSlider);

        tvPlot.setText(String.format(getResources().getString(R.string.label_plot), showIMDB.getPlot()));
        tvGenre.setText(String.format(getResources().getString(R.string.label_genre), showIMDB.getGenre()));
        tvRuntime.setText((String.format(getResources().getString(R.string.label_runtime), showIMDB.getRuntime())));
    }
}
