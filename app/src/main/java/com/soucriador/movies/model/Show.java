package com.soucriador.movies.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Show implements Serializable {

    @SerializedName("imdb_id")
    String imdbID;
    @SerializedName("title")
    String title;
    @SerializedName("poster")
    String poster;

    public Show(){
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
